# 第五周：正确排版入门

**排版**是一项再普通不过的技术，人人都认为自己会排版，也少见所谓「排版大师」或靠排版盈利者。但是，以上种种，仅是一种非常普遍的错觉。

在文本内容维持不变的前提下，好的排版有化腐朽为神奇之力，糟糕的排版则可以埋没最为天才的作品。而事实上，大家认为再简单不过的排版，其实有相当多的门道；「无师自通」是许多人的一厢情愿，大家只不过是习惯了低劣的排版而已。

> Typesetting is the composition of text by means of arranging physical types or the digital equivalents. Stored letters and other symbols (called sorts in mechanical systems and glyphs in digital systems) are retrieved and ordered according to a language's orthography for visual display. Typesetting requires one or more fonts ... One significant effect of typesetting was that authorship of works could be spotted more easily, making it difficult for copiers who have not gained permission. --- Wikipedia (English)
>
> 排版是通过排列字模（或其数字化形式——字形）以组成文本的技术，在此过程中根据语义、语法等选取并排列不同的字模（字形），以获得一定的显示效果。排版过程中，会使用到一种或多种字体……排版的一个重要作用就是使作品具备更加清晰的特征，阻碍未经许可的复制抄袭。——维基百科（英文，中文意译）

以上对排版的定义相对古典，大概来自于铅印时代。现在，排版的意义更加广泛，不再局限于「文本」的排列：公式、图片乃至多媒体，都成为了排版技术的处理对象。同时，排版也不再只是针对书籍、印刷品，在线文档、网站乃至应用都产生了对排版的需求。如我们前几周重点关注的「前端技术」，与排版技术便有非常多的重合；当网站的重点在于展现文本内容时，网站制作就变成了一项名副其实的排版工作。

本周课程中，将对我们在大学生活中可能涉及到的排版技术做一概括，并向大家介绍学术排版工具 LaTeX。尽管 LaTeX 是本课程的重点，但本周的安排其实是要传达另一种观点：排版工具的选择有很多，不必将 LaTeX 作为唯一的、彻底的选择。

## 1 从 Markdown 说起

Markdown 是一种「文档格式」，展示给人看的文档自然需要排版。Markdown 本身当然没有排版的功能（只是若干代码），这项工作由其转换而来的 HTML 文件在浏览器/应用中实现。由于 HTML 只负责说明「这个是标题」、「那个要强调」，而没有说明「标题应该长什么样」、「强调是加粗还是斜体」等具体的实现，因此准确的说还需要 CSS 样式表才能完成排版的工作。

以下演示如何将一个 Markdown 文件排版为书籍的模样。

### 1.1 第一步：编写 Markdown 文档

此处无需多言，唯有以下两点：

1. 善用各级标题，用它们区分内容层次；
2. 勿在 Markdown 中内嵌 HTML 标签或 CSS 样式表——它们会破坏文档的一致性，使排版工作陷入困境。（但如果用 Markdown 制作网站，此乃常见步骤。）

源代码 `test.md` 如下图所示，用记事本展示。如果你有比较好的编辑器、预览工具，现在你就可以看见「排版」之后的效果；不过，在这里，我们要自己来做这件事情。（此处用作者的一篇[博客](https://www.cnblogs.com/xjtu-blacksmith/p/rational-analysis.html)测试）

![markdown source](1.png)

### 1.2 第二步：转换为 HTML 文件

接下来，通过一个转换器将 Markdown 转换为 HTML。这样的工具有很多，而且你也已经见过：所有预览 Markdown 的工具都必然内置了这样的转换器，这样你才能看见美观的排版效果。此处用最常用来做转换的 pandoc 演示（第七周将专门讲解）。在命令行中执行命令：

```shell
pandoc test.md -o  test.html
```

即可将代码转换为 HTML 网页文件 `test.html`。在浏览器中看看这个网页：

![html file](2.png)

已经有可以看见的「样式」了，如引用块有缩进、标题与众不同等等。在第三周已提过，这是由浏览器默认附加的样式，只能满足基本的「区分功能」；但到现在，我们已经可以说，我们做了「排版」的工作——我们已经根据内容，以特定的方式排列了文本，这就是排版。

### 1.3 第三步：用 CSS 样式表优化排版

尽管已经初步实现了「排版」的功能，但我们还需要使得这个文档更加美观。例如，可以提出这样几条建议：

1. 更换字体为宋体 + Times New Roman——书籍中大多用此类字体排版；
2. 给页面增加页边距——书籍中都有，适当留白；
3. 字太密集了，适当增加字距；
4. 取消中文的斜体，将原来的斜体样式改为楷体字体——斜体在西文中比较搭配，用在汉字则极为别扭，不如改用其他字体实现「强调」的效果；
5. 把页面颜色改为淡黄色——模拟旧书；
6. 引用段落的字体也改改，变成斜体或仿宋，以与正文作区分……

类似建议还可提很多。它们无一例外是从「书籍」、「阅读」这样的需求出发的，这属于传统意义上的排版——以纸张为载体，电脑上不过是模拟纸质作品而已。当然，现在电子媒体也有了自己的排版风格，比如：

1. 使用黑体（包括微软雅黑、苹方、Noto Sans 等）一类的直线条中文字体，搭配无衬线（sans-serif）西文字体，富有现代感；
2. 采用黑白配色，偶尔用不同程度的灰色，强调内容；
3. 使用比较小的行距（line skip）搭配比较大的段距（para. skip），段落更加分明……

这算是广义的「排版」了，我们仍实现狭义者。现在，打开浏览器的开发者工具（或自己嵌入一个 CSS 样式表），加入以下规则分别实现上面所提六条建议：

```css
body {
    font-family: 'Times New Roman', '宋体';  /* 改变字体 */
    padding: 1in;  /* 设置页边距 */
    letter-spacing: 1pt;  /* 设置字距 */
}

em {
    font-style: normal;  /* 取消斜体 */
    font-family: '楷体';  /* 改为楷体字体 */
}

html {
    background-color: #FBF7DA;  /* 纸黄色背景 */
}

blockquote {
    font-family: '仿宋';  /* 引用字体更改 */
}
```

更改样式之后，浏览器中的显示效果如下：

![css effect](3.png)

可以看到，我们所希望实现的排版效果尽数实现。现在的样式仍然存在诸多问题，上面的样式代码也有一些瑕疵（比如，`em` 元素和 `blockquote` 元素没有备选的西文字体，其中的英文会比较难看）；为此，还可以补充更多的样式规则，美化其效果。

以上用样式代码来完善文档显示效果的工作，就是大家所理解的排版了：改样式、改功能。

### 1.4 小结

上面用比较「原始」的方法走过了 Markdown 文件排版的全过程。可以从中分析出排版的三个步骤：

1. 编写文本内容——本例中用 Markdown 实现；
2. 为不同的内容设置**格式**——本例中用 Markdown 与 HTML 实现；
3. 在不同格式的基础上设置**样式**——本例中用 CSS 实现。

大家经常只把排版理解为最后一步，即样式的设置；须知第二步的格式设置也非常重要。在排版过程中，最重要的原则就是「**内容与样式分离**」——这样，你可以为同一套格式设置不同的样式，轻松实现「主题切换」、「一稿多版」等功能。

## 2 解析 MS Word

说到排版软件，很多人脑子里蹦出来的必定是 Word。Word 确实是一款跨时代的应用，它最大的优势就在于：让小白在电脑上排版成为可能。当然，Word 也同样可以实现很专业的排版。（Word 究竟好不好，本课程中不讨论，请大家到以下指定场所去~~吵架~~发言：知乎、XX论坛……）

关于 Word 文档，一个基本的概念是：它是二进制文档，用记事本打不开。不过，用某些方式，也可以一窥 Word 文档的内部结构：

![word2xml](word2xml.gif)

以上动图说明了一件事情：将 `docx` 文件的后缀名改为 `zip`，你会惊奇的发现它不过是一个压缩包，打开后将得到若干 `xml` 后缀的代码文件。（当然，如果你的 Word 文档中嵌入了图片等外部资源，你也会在解压后的文件夹中找到它。）可以打开 `word` 目录下名为 `documents.xml` 文件（此处为展示层级结构，用浏览器打开）：

![xml file](4.png)

是否觉得这类代码十分眼熟？它采用了与 HTML 一样的标签语法，但所用标签个个陌生。这是 XML 格式的文件，XML 即 Extensible Markup Language 的缩写，意即「可拓展标记语言」。它的范围比 HTML 要广，只有对标签语法、层级结构的约定，其功能、层级组织方法等则完全不做要求——HTML 可以被看作是「用 XML 写网页」之特例，Word 所用的 XML 则是「用 XML 排版文档」之特例，又有一个新名字：OOXML（Office Open XML）。除了 Word 文档，你会发现 Excel 表格、Powerpoint 幻灯片也在使用类似的 XML 文档结构，都被归在 OOXML 的范畴之中。

根据以上的结果，我们可以得到一个初步的结论：

> Word 归根结底仍然是一种**标记语言**——机制仍然与 Markdown、HTML 类似。

为什么我们从未把 Word 当成一种标记语言，而是作为一个「专业的排版软件」呢？因为：

1. 为了实现精致的排版效果，Word 所用标记语言之结构过于繁复，且往往多个代码文件相互依赖、嵌套。由此导致其代码几乎不可能手工地识别、编写（看看上面的例子，你应该能理解）；
2. Word 所属的 Office 套装设计之初就与 Windows 系统紧密结合，它们的共同特点是：强调用户界面（「窗口」），竭力避免用户与底层机制、代码打交道；
3. 微软历史上还有一笔黑账，曾有人扒出 Bill Gates 要求 Office 团队把其文档设计得「其他软件都打不开」；虽然现在 OOXML 这种格式已经完全开放了，但还是继承了这方面的因素，有许多冗余的结构层次。

因此，以上的 hint 对你的帮助，并非使你能直接处理 Word 的底层代码，而仅是帮助你对 Word 的本质有更多的认识。由于 Word 归根结底是一种标记语言，因此它也就具有标记语言的种种局限（下面再谈），进而影响到排版效果。

## 3 Alternative?

根据我们已有的知识，Markdown（或 HTML）、Word 等等都是标记语言。用它们来做排版的工作，优势是很明显的：

1. 容易实现「样式与格式（内容）分离」，由标记语言来实现格式，由类似样式表的机制来实现样式；
2. 规则简单，学习容易；
3. 结构、层次清晰，可以比较方便的互相转换。

当然，Word 很多时候是例外，因为大家使用时并不接触其源代码，亦不深入学习其使用，导致恶习丛生（比如：手动设置每一个标题的样式）。

有优势，自然也有劣势。譬如 HTML 文档，我们已经演示了其排版效果，有心者自然会提出以下几个无法回答的疑问：

1. 一般书籍是分页的，在 HTML 中无法实现，如用 JavaScript 脚本来做则破坏了文件结构；
2. 目前的浏览器、应用对分词、分行、断页等比较专门的问题处理不好，导致阅读效果不佳；
3. HTML 或 Word 文档中的字体，在缺少该字体的设备上无法正常显示（通常会用其他字体替代，比如宋体——效果「感人」）；
4. 标记语言对于数学公式、矢量图表等书籍中常见的对象支持不佳，脚注、索引等一些书籍中常用的功能也难以实现。

你也许会说，以上的需求有些「鸡蛋里挑骨头」；事实上，它们是真实存在的需求。归根结底，问题在于：**标记语言并不是专门为排版而生**。要一个制作网页的工具来模拟书籍、论文的效果，的确勉为其难。

那么，有没有专门用来排版的工具呢？当然有，大名鼎鼎者如 Adobe InDesign、Microsoft Publisher、苹果的 Pages 等等。它们是商用软件，有相应的操作流程，需要专门学习——总体来说，都是可视化界面，是点点鼠标的工作。它们与标记语言处在对立面：专业、复杂、不易转换格式。

现在自然要问：有无「折衷」的选项？TeX / <latex></latex> 也许算——通过相对简单的源代码，实现专业的排版质量。以下展示了对同一个段落（[这说的是什么？](https://cn.lipsum.com/)），Word 与 <latex></latex> 的排版效果；可以看到，<latex></latex> 中的词距控制得更加均匀合理。（结合图中信息，以及你使用 Word 的经验，你可以初步想想这是为什么。）

![Word vs LaTeX](5.png)

至于 <latex></latex> 是什么，TeX 又是什么，它们是什么关系，都留作问题，在你之后的学习过程中去解决。

## 4 由 Markdown 跨向 <latex></latex>

与 Markdown 不同，<latex></latex> 不能被简单地视为一种标记语言（加以种种限制之后，才有可能）。它有悠久的历史（比 Markdown、HTML 乃至 Word 等都古老），有与标记语言完全不同的发展动机与路径，有非常复杂的底层机制（<latex></latex> 是**图灵完全的**——换句话说，C++、Java 等编程语言能做到的事，<latex></latex> 在理论上全都能做到）。因此，想要用好 <latex></latex>，专门的学习是不可避免的。

但是，由于诸位学过 Markdown，开头的一段就有捷径可走了。照理来说应当从 <latex></latex> 的发展历史、第一份文档（Hello world 之类）学期，现在我们可将它们统统放在一边，一开始就写一份「有声有色」的 <latex></latex> 代码。

在第一节课中，曾经提及：Markdown 功能不多，却涵盖了 90% 乃至 99% 的一般需求。那里归纳为「八大金刚」，如加粗加斜、标题、引用等。现在，我们首先写一份简单的 Markdown 文档，命名为 `lipsum`：

```markdown
# Lorem ipsum

*Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.* Ut enim ad minim veniam, quis nostrud **exercitation** ullamco laboris nisi ut aliquip ex ea commodo consequat.

> Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
```

简单应用了标题、加粗、加斜、引用四种格式。在 <latex></latex> 中如何实现它们呢？既然不会写，先让文档转换工具去做这件事。执行以下命令来转换（你的电脑上没有 `pandoc`，不用试了）：

```shell
pandoc lipsum.md -o lipsum.tex
```

得到一份名为 `lipsum.tex` 的 <latex></latex> 代码（记住后缀名）。看看里面的内容：

```latex
\section{Lorem ipsum}

\emph{Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
eiusmod tempor incididunt ut labore et dolore magna aliqua.} Ut enim ad
minim veniam, quis nostrud \textbf{exercitation} ullamco laboris nisi ut
aliquip ex ea commodo consequat.

\begin{quote}
Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
\end{quote}
```

通过对照原文、一一对应，我们能够发现 <latex></latex> 中对应的命令：

- 一级标题：`\section{xxx}`（`xxx` 表示格式内容，下同）
- 斜体：`\emph{xxx}`
- 粗体：`\textbf{xxx}`
- 引用块：`\begin{quote}xxx\end{quote}`

跟 Markdown 比起来，这种写法显然是要啰嗦些；不过想到 HTML 的标签语法，心情一下子又会平复。~~这告诉我们：人比人，气死人。~~可以进一步归纳几条规律：

1. <latex></latex> 中的命令都是用反斜杠 `\` 开头的。（你也许会想到 C 或其他编程语言中的转义字符。）
2. 简单命令的形式为 `\abc`，其后有一对大括号包裹命令的「参数」或「执行对象」。
3. 还有一类复杂的命令，形式似乎是 `\begin{abc}...\end{abc}`（它们的定界符仍然符合「简单命令」的形式），其中包括被执行命令的文本内容。——这在 <latex></latex> 中叫做环境，是一种常见的结构。

好，恭喜你，你已经学会了别人翻书、鼓捣一整天才能得来的三条重要信息。以上三条规律中，除了第二条不总是成立之外（大部分情况下都成立），基本概括了 <latex></latex> 中的命令结构。

> 命令是什么？你可以类比地理解为标记——它不是文本，因此不会以原样展示在文档结果中。比如 `\section` 在最终的文档中一定不会显示为 section 这七个字母。

不过，以上的 <latex></latex> 文档还不能够编译出可见的文档，因为它只是内容——相当于 HTML 文件中的 `body` 元素或编程语言中的 `main` 函数，还缺少一些「部件」。因此，你还需要继续了解 <latex></latex>。

## 5 怎样正确学习 <latex></latex>

钱院学辅的 <latex></latex> 教学有一惯例：不讲语法细节，请诸位自己看书、尝试。这是因为 <latex></latex> 不比 Markdown，内容繁多，讲两个小时也未必入门——而且很可能带人走上「歪路」。但是，仍然需要一些基本的指导——百度「<latex></latex> 教程」、「<latex></latex> 入门」，所得的搜索结果十有八九不靠谱，这是一个过来人的真实感受。

> 如果你对 <latex></latex> 的需求是应急、交差，这些教程倒也称心合意，因为其作者大多也是抱着这种心态在学 <latex></latex>。

还好，现在至少有一条足称「正确」的学习路径。注意，以下内容不是我一人或几个人的「经验之谈」，它是整个 <latex></latex> 社区归纳而来的建议。

### 5.1 如何正确入门 <latex></latex>？

1. **下载最新的 TeX 发行版软件**，一般用 TeX Live（Windows & Linux）或 MacTeX（苹果）。下载链接，请到各类镜像站上找，[这个页面](https://tex.readthedocs.io/zh_CN/latest/sections/3-appendix_b.html)上也有整理。
2. **安装**。对初学者来说，找到那个可执行的 `install` 或 `setup` 文件，无脑执行即可（不需要做任何「定制」）。一份名为 [install-latex](https://gitee.com/OsbertWang/install_latex/releases) 的教程给出了更多说明（主要是命令行操作），**若有命令行使用经历**也可参考。
3. **阅读唯一推荐入门教材**：`lshort-zh-cn`。在命令行中执行命令 `texdoc lshort-zh-cn`，即可自动打开这个 PDF 文件；你也可以多折腾一道，再下载一个：[下载地址](http://mirrors.ctan.org/info/lshort/chinese/lshort-zh-cn.pdf)。阅读建议在后面。
4. **准备一本参考书**，因为以上教材只能帮你入门。刘海洋的[《<latex></latex> 入门》](https://book.douban.com/subject/24703731/)是很好的参考书，可备在手头，用来查询未知内容。
5. **学会解决问题**。已经反复强调，TeX / <latex></latex> 机制复杂，因此你会不可避免地碰到新问题。这方面也有许多文章可写，暂无合适者；[这个页面](https://tex.readthedocs.io/zh_CN/latest/sections/1-learning_route.html#id2)所展示的「群里提问的艺术」可作侧面参考，特别是其中的「提问之前」部分。

### 5.2 学习中有哪些注意事项？

1. 把 *CTeX 套装*（勿与 CTeX 宏包弄混）和 *CJK 宏包*视为禁忌，它们过时久矣。包含这两个内容的教程、介绍一律关掉，绝不要安装使用。
2. 慎用网络资源，不要随意抄用他人给出的代码。
3. ~~不要在学习过程中飘飘自得，也写一份「<latex></latex> 入门教程」。~~

## 6 任务

本周任务，应该是学习 <latex></latex>。但据统计，85.714285% 的人无法在一周内比较好地入门 <latex></latex>，所以这个任务有些困难。

为此，本周布置这样一个特别的任务：请找到第一周时你所完成的任务二之 Markdown 文档，将其**转换**为一份 <latex></latex> 文档。最终要提交编译得到的 PDF 文档。由于那份文档中实现了「八大金刚」的功能，因此这次任务的意义就是：在 <latex></latex> 中实现「八大金刚」。

- 关于「转换」：如果你学会了使用 `pandoc` 一类的转换工具，并无不可。当然，还是建议你根据所学的 <latex></latex> 知识，手工实现对应的命令。
- 关于样式：Markdown 中的格式与 <latex></latex> 中的格式，可能会有不同的显示效果（样式）——比如 Markdown 中的引用块往往展示为左侧有灰线的灰字段落，而在 <latex></latex> 中的引用块（`quote` 环境）默认展示为缩进、上下隔开的普通段落。**你只需要关注格式，不需要关注最终的显示效果有何差异。**
