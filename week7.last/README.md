# 第七周：最后一课

公开课将近尾声，这周介绍一个比较重要的内容：文档格式转换。另外，画几张大饼，请你看看自己学了这些东西之后能干什么，看看能不能获得一种~~虚假的~~成就感。

## 为什么要做文档格式转换

回顾之前所学内容，你学到了哪些新的「语言」呢？大概有：

- Markdown
- 前端技术：HTML、CSS
- XML（包括 MS Word）
- TeX / <latex></latex>

它们中的每一项，都有特定的用途。简单说来：

- Markdown 是最简单的一个，最适用于**创作**，如写笔记、写普通文章等。
- HTML 就是网页的同义词，当然适合作**线上展示**的载体；也是 Markdown 的转换终点。
- XML 比 HTML 的范畴更广，可做**数据记录**等多项工作。
- MS Word 最**通行**，很多时候别人给你的文档都是 Word 格式，有时也只允许你提交 Word 格式的文档。
- <latex></latex> 在**高质量排版**（整体的协调、美观程度）、**学术排版**方面仍然独步，其**公式系统**则应用范围更广；由其编译而成的 PDF 文档也具有很多优势。

这些典型的用途，正是由它们的优势决定的。因此，这里可以套一句官话：「我们要取其精华、去其糟粕，扬长避短，坚决发挥我们的聪明才智，选用最合适、最正确的格式，完成编写文档的伟大光荣使命。」

为了某一种格式的底稿在不同场合发挥优势，便非常需要做格式转换。当然，最理想的情况是：用最简单的 Markdown 格式做底稿，然后转换为其他任意格式。

## 关于 Markdown 引擎

首先是 Markdown 至 HTML 的转换，这几乎是每一个 Markdown 文档的宿命，因为 Markdown 在设计之初便是如此考虑。坊间所言「Markdown 引擎」，也无一例外地都是 Markdown 到 HTML 的单向转换工具。

也许你会想：Markdown 到 HTML 有什么难度？它们的元素不是一一对应吗，替换就行。说起来简单，做起来难；特别是在比较复杂的情况下，对一份 Markdown 文档的解释很有可能产生歧义，甚至是出错。

### 可能产生歧义的实例

以下给一个 Markdown 片段（Exp. 31 in Commonmark Spec）：

```markdown
- Foo
- * * *
```

这里很有可能产生歧义：列表第二项的 `* * *`，是应该解释为一个水平线，还是应该解释为普通的文本？如果认为其是水平线，那么它是应该继续作为列表项，还是直接从列表中分离出来（因为水平线不应该出现在列表中）？又或者，认为第二个列表项不甚合理，直接报错？又或者，认为第二行不过是一个正常的换行（大多数 Markdown 引擎中认为换一行不算分段），所以干脆将第二行拼回第一行？

你也许会认为：这种奇怪的写法不应出现，以上的分析也纯属多情。事实是：上面所说的每一种解释，都是存在的，存在于不同的 Markdown 引擎中。

- 第一种：将第二行看成一个作为列表项的水平线。尽管这很别扭，却是大多数 Markdown 引擎的解释结果，如 GFM、Commonmark、Kramdown 等。
  ```html
  <ul>
    <li>
      Foo
    </li>
    <li>
      <hr>
    </li>
  </ul>
  ```
- 第二种：将第二行看成是列表之外的一条水平线，认为开头的 `-` 号和后面的 `*` 都是在表示水平线。Maruku（一个曾经很热门的引擎，现已停止更新），采用此种解释。
  ```html
  <ul>
    <li>
      Foo
    </li>
  </ul>
  <hr>
  ```
- 第三种：认为第二行就是一个正常的列表项，`* * *` 没有深意。RedCarpet（跟 Maruku 性质类似，几年前很火）采用此种解释。
  ```html
  <ul>
    <li>
      Foo
    </li>
    <li>
      * * *
    </li>
  </ul>
  ```
- 第四种：认为第二行就是个普通的换行，和第一行拼一起完事。一款名为 lunamark（用 Lua 语言编写）的引擎采用此种解释。
  ```html
  <ul>
    <li>
      Foo - * * *
    </li>
  </ul>
  ```
- 第五种：报错。一款名为 blogdown（用 Haskell 语言编写）的引擎是这样做的。不过它的报错原因令人意外：第二行有三个 `*`，当成了斜体标志（哪怕里面是空格），少一个没封闭。
- 第六种：来自 Markdown 创始人的引擎，惊不惊喜？看看它怎么解释的：
  ```html
  <ul>
    <li>
      Foo
    </li>
    <li>
      <ul>
        <li>
          <ul>
            <li>
              *
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  ```
  它把第二行的四个符号 `- * * *` 之前三个都看成列表标志，层层缩进，最后一个 `*` 才看成列表内容。

六种解释图示见下，它们均来自网站 [BabelMark3](https://babelmark.github.io/?text=-+Foo%0A-+*+*+*) 的编译结果。在这个网页上，你会发现解释其实不止六种……

![parsing results](1.png)

如果只着眼于文档的内容，考虑到一般所用的格式，我们无疑会感到这个例子有些刁钻；但从中来看，不得不承认，Markdown 到 HTML 这基本的转换也绝非易事。类似上面的「复杂」情形，事实上还能举出许多，不多谈了。

### 主流引擎推介

那么，为了防止出现意料之外的情况，比较稳妥的方案是什么呢？一条建议：使用**主流**的 Markdown 引擎来做转换。所谓主流，大致是指：应用范围广，更新及时，（至少在一特定领域、应用场合）普遍为人接受。这样的引擎包括：

- CommonMark：一个立志做 Markdown「国际标准」的引擎/语法体系，细节做的出色，上面的例子就是从其[语法文档](https://spec.commonmark.org/current/)中摘出的。缺憾是未包含拓展功能，如表格、脚注等都一律没有，原因大概是因为其想把这些功能排除在「标准」之外，留给其他引擎去填补。
- GFM（GitHub Flavored Markdown）：GitHub 上通行的引擎/语法，在 CommonMark 基础上增加了表格、checkbox（打勾框）等一点拓展功能。一般的应用、网站，如果对 Markdown 引擎没有特别说明，基本上都是 GFM 引擎/语法，或是其拙劣模仿。
- Kramdown：用 Ruby 编写的 Markdown 引擎之最好者，有多种实用拓展，最适合的应用场景是**用 Markdown 做网站**时。学辅网站现用 Kramdown。
- Pandoc Markdown：Pandoc 工具内置的引擎，拓展语法很多。
- MultiMarkdown (6)：一个不温不火的引擎，拓展功能多，支持多种格式输出，最适合的应用场景是**想把 Markdown 用成排版工具、让网站变成在线书籍**时。

以上五者，自然前二者最稳妥，后三者在一定范围内可用。其他的奇怪引擎，看看即可，尽量不用。此外，还有一个可能的问题：你不知道某个网站、应用的 Markdown 引擎是什么。这个情况极常见，有时候也会让你很头痛，目前来说只能是「吃一堑，长一智」，或发信给网站、应用的管理人员问问看，也许会有回音。

### 养成良好的 Markdown 编写习惯

上面是一条建议，还有一条建议：写规范的 Markdown 文档，限制自己的行为，这样也能减少意外。相信你不会写如上面例子一样的奇怪格式，但你可能会犯其他一些坏习惯，比如：

- 写标题时，`#` 号和标题文字之间不留空格：大多数引擎能够容忍这种行为，但仍有一些会出错；
- 不习惯分段，分段时只按一次回车：有些引擎会将连续文本之间的换行识别为默认换行，这会给你一种「按一次回车」就能另起一段的错觉，这不好（难看，段距不匹配）。有人甚至直接用 `<br>` 标签（强制换行）分段，令人无语。
- 写列表时，时而空行，时而不空，像这样：
  ```markdown
  - foo
  - bar

  - foobar

  - barfoo
  ```
  在 Markdown 的列表项间，空不空行皆可，但应当统一，最好一篇或一系列文章都统一。原因是：一些 Markdown 引擎会区别对待有无空行的列表项（如 Kramdown），有空行者会将列表项外附加一个 `p` 标签，有可能产生不均匀的间距。
- 在标题的内容中嵌入代码、粗斜体等格式：一些引擎可能处理不好这种情形。
- 缩进混乱：4 个缩进会被当作代码块处理，破坏你原来期望的样式。

诸如此类的点还可以提很多，但都是零碎的，不能尽全。在这方面，没有什么「典范」可言，需要你自己去摸索。此外，还有一些仿效编程语言的语法检查器而设计的 Markdown Linter（如 VS Code 中有名为 [markdownlint](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint) 的插件），也能规范你的习惯——虽然它们制定的规则未必合适。

## 文档转换工具：Pandoc

Markdown 转换 HTML 没话说，到处可以。Markdown 转其他格式呢？需要专门工具。上面所提即的三个「引擎」：Kramdown、Pandoc、Multimarkdown，都支持多格式输出，而以 Pandoc 最强大——真正志在「万物互联」。下面是 Pandoc 支持的格式一览（[原图](https://pandoc.org/diagram.jpg)）：

![pandoc support](2.png)

~~此图作者一定是对密集恐惧者患者怀有严重恶意。~~图够吓人，不够明晰。看看列表描述：

![pandoc list](3.png)

红框标出来的，是这门课程结束后你应当认识的东西；其他内容，也是你未来可以开拓的疆土。有了 Pandoc，靠着类比的观点和行为，学习其他的文档格式（通常是在特定领域有用）会更加轻松。Pandoc 强大至此，无需怀疑了——自称「瑞士军刀」。

当然，好用和强大，未必统一。一个特征是：强大工具，往往都是命令行下用，Pandoc 也如此。命令行工具也有好处：能用程序调用，掌握熟练之后能比鼠标点击高效百倍。初学之时，对命令行不熟悉，则还是一点一点来。

[Pandoc 官网](https://pandoc.org/)国内访问流畅，但是[下载](https://pandoc.org/installing.html)要走 GitHub，可能有问题。老办法，这里给一个[百度网盘](https://pan.baidu.com/s/1N_TH8ix7aDTchxZ8XWtIKA)的备用链接（passcode: ria4），比比看谁快。

> 在 Windows 系统上，还有一个已知途径：安装 Anaconda，因为其中的 Jupyter Notebook 依赖 Pandoc 来将笔记本导出为其他格式。（但是 Anaconda 发行版中的版本偏旧，你可以考虑手动升级。）因为 Anaconda 有国内镜像，所以这条路是通畅的；如果你已经因不同目的安装了 Anaconda，则不需要再安装 Pandoc，打开命令行痛快吧（可能要配环境变量）。
>
> 为了一个 Pandoc 安装一大套 Anaconda，是下策。你可以跑到镜像站的 Anaconda 库里去找到 Pandoc 的文件，解压即用，也挺痛快。（比如清华上的 [pandoc 2.2.3.2 x64](https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/win-64/pandoc-2.2.3.2-0.tar.bz2)，版本略旧，并且要配环境变量，可折腾看看。）
>
> 如果是苹果或 Linux 系统，可直接用包管理器，快捷省事。

安装完了之后，测试一下能不能用——打开命令行：

```shell
pandoc -v
```

显示一大段版本、路径、软件信息，则说明安装正确。

### 基本用法

如果你不想管太多，可以直接上起手来写：从文件 A 到文件 B。比如，要将 `test.md` 转换为 `test.docx`，就运行：

```shell
pandoc test.md -o test.docx
```

其中的 `-o` 参数表示输出。一般来说，不会有什么问题，只是未必合你心意。下面说说细节。

### Markdown 转换

Pandoc 支持好几种 Markdown 语法，包括：

- CommonMark
- GFM
- 原始 Markdown（就是上面那个例子里摆「人字形」的）
- MultiMarkdown
- PHP Markdown Extra（最早搞拓展功能的）
- Pandoc('s) Markdown

支持好几种，与我们无关，我们选用自己习惯的那种（一般来说是前两者之一）就行。问题是：如果你像上面那个「无脑」命令一样不给参数，Pandoc 默认用最后一种——自家语法 Pandoc Markdown。其拓展甚多，原因是 Pandoc 跨越太多中文档格式，希望也有一种 Markdown 语法能包括各种文档的特性（以免转换过程中流失、出错）。

当然，Pandoc Markdown 与其他基本 Markdown 语法的关系是「向下兼容」，在简单的用法上无歧义，所以如果是用 Markdown 文件转换到其他文件格式时不用太担心。反之，若要将一份 <latex></latex> 代码或 Word 文档转换为 Markdown，则要小心，最好附上参数指明 Markdown 格式，用 `-t` 参数紧跟其名：

```shell
pandoc -t gfm test.tex -o test.md  # 要求采用 GFM 语法
pandoc -t commonmark test.tex -o test.md  # 要求采用 Commonmark 语法
```

否则，生成的 Markdown 文件可能有一些只有 Pandoc 才支持、其他引擎均不支持的标记。

### 输出 <latex></latex>，及模板

Pandoc 也支持 <latex></latex> 格式。不过，在第六周时曾演示过，直接像上面用 `-o` 参数转换 Markdown 文件，得到的 `.tex` 文件中只有 `document` 环境以内的内容（你可以[点这里](../week5.typeset#由-markdown-跨向-latex)回去看看），不能直接编译。为了获得可编译的 <latex></latex> 代码，你只需要加一个参数 `-s`（完整写法是 `--standalone`）：

```shell
pandoc -s test.md -o test.tex
```

无忧了，正常编译。当然，建议你不要去检查这个转换出来的、可编译的 `test.tex`，否则你可能会在导言区看到以下内容：

```latex
\PassOptionsToPackage{unicode=true}{hyperref} % options for packages loaded elsewhere
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provides euro and other symbols
\else % if luatex or xelatex
  \usepackage{unicode-math}
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage[]{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\usepackage{hyperref}
\hypersetup{
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\date{}
```

本来还想改改样式，一看这架势——告辞。这个骨架确实不敢随意动，其中有很多是编译时的必须项；由这套命令生成的格式、样式，除了字体之外，都和典型的 Markdown 文档比较相近，如标题不编号、段首不缩进、有段距、列表间距小等，这些是与 <latex></latex> 默认样式不同的。

如果你想要用自己的样式、命令，也没有那么困难，只要使用**模板**功能就好。你可以从 Pandoc 官方文档的[模板](https://pandoc.org/MANUAL.html#templates)一节慢慢读、了解模板的语法；如果嫌慢，可以直接从默认模板上开始改，只改样式。首先要获取到当前的默认模板，通过下面的命令：

```shell
pandoc -D latex > default.tex
```

将默认模板导入到当前目录下名为 `default.tex` 的新文件中。打开看看，附加自己的样式规则就好，存起来。初看时可能会头晕目眩，其实里面大多数内容都不会用到——都是 `$if$` 之类的命令，条件满足时才会起效。

使用模板，则要用到 `--template` 参数（很不幸，没有缩写）：

```shell
pandoc test.md -o test.tex --template=default.tex  # 采用 default.tex 作为模板
```

在这里要注意路径别写错，怕错就和待转换的文件放在同一文件夹下。最好把自己写好、常用的模板存在容易访问到的目录下，以便调用。

### <latex></latex> 文档中文问题

一个必须解决的问题是：pandoc 生成的 <latex></latex> 文档，**不能正常编译出中文**；因为其默认模板中，没有添加任何对中文的支持。最简单的解决方法是：在默认模板的基础上加一句 `\usepackage{ctex}`，万事大吉。这句话可以放在模板导言区任何合适的地方，越早越好。

另一种解决方案是：不修改默认模板，而在 Markdown 文件中加入一个 YAML 头（就像在 Jekyll 中一样）：

```markdown
---
documentclass: ctexart
---

# Hello, world ...
```

这里采用了模板变量的机制，将文档类变量设置为支持中文的文档类。你也可以换成 `ctexbook`，或自己写一个简单的程序自动为一个 Markdown 文件加上这个 YAML 头。

如果你除了中文之外还有更多需求，可以考虑自己定制一个更加合适的模板。慢慢来。

### 其他功能

除了以上所提的 Markdown 转 HTML、Markdown 转 <latex></latex> 之外，还有一些常用的特性，比如：

- Markdown 转 PDF：一般是通过本地的 <latex></latex> 引擎来做中转，即先转为 <latex></latex> 再编译得 PDF 文件。默认用 `pdflatex` 方式编译，如果是中文文档应指定用 `xelatex` 方式编译，并按上面所言事先准备好支持中文的 <latex></latex> 模板文件：
  ```shell
  pandoc book.md -o book.pdf --pdf-engine=xelatex --template=xxx.tex  # 采用 xelatex、有中文支持的 xxx.tex 模板编译
  ```
  当然，如果你对 <latex></latex> 没什么执念，或者你的电脑上一时没有 <latex></latex> 环境，也可以通过其他渠道获得 PDF，包括 `wkhtml2pdf`、`prince`，虽然它们也需要再单独安装。Typora 编辑器中可「如假包换」地快速导出 PDF，目前还没弄清其原理何在。
- Markdown 转 Word：直接用基本的 `-o` 参数即可，样式模板估计不会令你满意。由于 Word 文件本身结构复杂，所以 Pandoc 对 Word 没有像 <latex></latex> 一样的「模板文件」，而是通过名为参考（reference）的机制从一个指定文件中提取样式，再复用到其他导出的 Word 文档中。请通过[这个自制视频](https://www.bilibili.com/video/av88756766/)了解如何实现。

如果还需要了解更多细节，只有去认真阅读[官方文档](https://pandoc.org/MANUAL.html)了；详尽至极，可惜没有人持续跟进做翻译。 ~~我在这里先插个旗：有想跟进翻译 Pandoc 文档的，请联系我，总人数达到 10 个（含我）后我们就开搞，一定造福全国人民。~~在此意义上，文艺、技术有一个共同点：总体水平贫乏之时，翻译的意义比创作大。

## 总结：展望未来

到此，技术小组公开课的内容大致都已讲完——至少是主干。有一些分支，没有提，因为内容深，偏离主线太远。

主线是什么呢？其实很清晰：从 Markdown 开始，经 Markdown 到 HTML 的类比、转换而讲了前端技术，再撤回来由 Markdown 到 <latex></latex> 的类比讲了 <latex></latex>。最后用广泛意义上的文档转换收尾。同时，因为实际需要，也提了一点相关的「题外话」：Git 与 GitHub、码云。

我自己认为，Markdown、前端（若不涉及脚本，其实脚本也可辅助、美化内容）、<latex></latex> 排版，三者关系很近，但不能等同，各有一片天地。本课程主要是讲它们三者之关系近，讲的是共性，方便大家理解、入门；至于它们各自的那片天地，本课程无力涉及，大家有闲心、有闲功夫时可自己深挖：

- **Markdown**：不知道你对之前粗略涉及的歧义现象是否有兴趣。你也可以尝试着编写、发展一个有特点的 Markdown 引擎；
- **Git & GitHub**：别的不说，GitHub 上的东西够你看了。~~不管你做不做的出来新东西，至少你可以从此少浪费一些时间，做别人已经做过、做好的东西。~~
- **前端技术**：在十到二十年前，前端还处于 IT 界的下游位置，因为当时 HTML、CSS、JavaScript 三者都很粗糙、难担大任。现在，前端不仅蓬勃发展，甚至已经开始取代其他一些领域：桌面应用程序、在线服务平台，现在也可以主要地靠前端来完成，夸张而言就是「会做网站就会做软件」。本课程只把前端视为呈现内容的一种方式，其实还有很多条路可走——而入门的门槛，都不太高。
- **博客工具**：除了我们着重讲的 Jekyll，乃至类似的 Hexo、Hugo 外，做个人网站还有很多思路，比如用前端框架（近来以 Vue 框架为基础的 Vuepress 有些热闹），比如用脚本和数据信息自动生成网页，都是很有意思的事情。
- **自建服务器**：从「搭建博客」这个需求开始，你可以慢慢学会与服务器打交道，并在建网站之外尝试更多有趣的工作。
- **TeX / <latex></latex>**：TeX 作为一个快五十年前诞生的「古董」，为什么仍享有如此崇高的地位、无可取代？我已经见过很多不同技术领域的大牛，一面对 TeX / <latex></latex> 繁琐（他们的说法是「丑陋」）的语法、未经封装的系统机制等深表不满，另一方面又承认在排版质量方面还从未出现过够资格的挑战者。我们作为初学者，也许无力探讨这个问题，但至少知道一件事情：深挖这项技术，稳赚不赔。如果你对实在的东西更感兴趣，那就认一个理：冠冕堂皇的专家、审稿人，在评审论文时也免不了「看脸」。（这一方面有研究成果证实，可惜暂时找不着出处。）
- **文档转换工具**：往上翻翻，看看那幅图里密密麻麻的连线，你就知道其实想找一件事情做一辈子并不困难。~~当然，我们肯定不会做这种蠢事。~~

上面说的工作方向，大多数以「兴趣」为前提。如果你到现在对所学内容都不感兴趣，也没关系，可持更加现实的态度：

- Markdown：**节省创作时间，时间就是金钱！**
- Git & GitHub：听说国外（如美国）看法已变；但在国内，**有光鲜的 GitHub 账号也更好找（相关行业）工作！**
- 前端技术：**做网站也没那么难，逼格一下子就上来了！**（另有传说，可以用你的 NPY 名字注册域名，做一个网站献给 TA；我觉得这种事情很蠢，一个 NPY 哪值得这么折腾。）
- 博客工具：**远离 CSDN 苦海，享受清净生活！博客工具还可做简历，也很有用！**
- 自建服务器：**开辟私人空间，顺带学学 Linux 系统，提升阅历！**
- TeX / <latex></latex>：前面已经说过，**让论文更光鲜，铺平学术之路！**
- 文档转换工具：**逃离 Ctrl C V 苦海，天文数字工作量瞬间完成，爽！**

如果你看了上面的文字觉得难堪，也好，说明你还不算彻头彻尾的俗人 ~~，还有机会修成正果~~。若要问我是追求兴趣或直面现实，我则回答：都不是M权当是周游「技术世界」，长些见识。若不嫌弃，也可以持和我一样的态度。

这周没有任务了，结束。望你将来月入百万时，能偶尔想起远在边疆某锅炉房铲煤的我有一份功劳。仅此。
