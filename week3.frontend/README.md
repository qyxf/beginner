# 第三周：前端初步

> 前端开发是创建 Web 页面或 app 等前端界面呈现给用户的过程，通过 HTML，CSS 及 JavaScript 以及衍生出来的各种技术、框架、解决方案，来实现互联网产品的用户界面交互。它从网页制作演变而来……
>
> —— 百度百科「前端开发」词条

在初步学习「前端」时，可简单将其理解为「制作网页」，包括其内容、布局、样式、交互功能等。

## 1 网页的组成

简单的网页可由三个部分构成：内容布局、样式、脚本代码。

### 1.1 内容及布局：HTML

超文本标记语言（Hyper-Text Markup Language, HTML）用于组织网页的内容与布局，其中**标签**是理解 HTML 的关键概念。例如，以下是一个最基本的 HTML 代码文件：
```html
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <title>hello world 页面</title>
  </head>
  <body>
    <h1>你好</h1>
    <p>世界！</p>
  </body>
</html>
```
其中各种用 `<...>` 尖括号包围起来的称作**标签**（tag），它们往往成对出现（例如 `<html>` 与 `</html>`）。HTML 中的任何内容都必须：

- **作为**标签，如以上的 `<!DOCTYPE html>` 自身即为一个标签；
- 或被**包含**在某个标签之内，如以上的 `hello world 页面` 文本即被包括在一对 `<title>` 标签以内。

此外，在标签中往往还有很多附加信息（如以上的 `html` 标签中就有 `lang="zh-CN"`），它们被称作标签的**属性**（attribute），相当于是标签的变量、参数。

在 HTML 中，标签所起的作用，如同在 Markdown 中各个特殊符号（如 `*`、`#`）所起的作用：指定**格式**，为原本平凡的文本内容赋予不同的意义、作用（如作网页的标题、正文、附加信息等）。

标签内既可容纳原始的文本，也可容纳其他的标签，后者使 HTML 能够形成非常有序的层级结构。典型的 HTML 文件中一般有如下层级（用列表的缩进示意嵌套、包含关系）：

- 顶级的 `html` 标签，除了个别特殊标签外的其余标签都必须包含在内；
  - `head` 标签，其中可用 `meta` 或其他特定标签声明文件的辅助信息，如字符集、网页标题（`title` 标签）等；样式表、脚本的引用代码也常放在此处（非必须）。一般而言，`head` 标签中的文本**不会**显示在网页上（包括标题，`title` 指定的标题会显示在浏览器顶部，而非网页中）。
  - `body` 标签：网页内容与布局基本都在此标签中实现。`body` 中的内容一般都会显示在网页上。
    - 不同的文本格式标签，如上一例中的 `h1` 标签即是一级标题（在 Markdown 中通过一个 `#` 号实现），`p` 标签则是普通段落。
    - `div` 标签，代表一个长度、宽度等可以自行指定的「容器」，常用若干 `div` 来组织网页的布局。
    - 其他格式标签，如 Markdown 中的各类格式在 HTML 中均有实现。

与 Markdown 中寥寥数种格式构成的「小工具箱」相比，HTML 中可实现的功能非常之多，不可能也没有必要全部掌握。可以从与 Markdown 有**对应关系**的几种标签开始了解：

- 普通文本段落（paragraph）：`p` 标签
- 加粗：`strong` 标签
- 加斜（emphsize）：`em` 标签
- 各级标题（headings）：`h1`～`h6` 标签
- 分割线（horizontal rule）：`hr` 标签
- 引用（块）：`blockquote` 标签（行内引用可用 `q` 标签，不实用）
- 无序列表（unordered list）：`ul` 标签（列表框架） + `li` 标签（嵌套在内的列表项）
- 有序列表（ordered list）：`ol` 标签（列表框架） + `li` 标签（嵌套在内的列表项）
- 代码块：行内为 `code` 标签，行间为 `pre` 标签（此处的 pre 表示「可预处理样式」）
- 链接（锚点 anchor）：`a` 标签
- 图片（image）：`img` 标签

欲了解更多关于 HTML 的信息，可以从[菜鸟教程](<https://www.runoob.com/html/html-tutorial.html>)出发。了解基本概念后，可针对具体的标签、语法再做更多了解。

HTML 文件承载了网页的主体，网页浏览器就是 HTML 文件查看器。

### 1.2 样式：CSS 样式表

以上一个 HTML 文件，在浏览器中的显示效果如下：

![img](1.png)

可以看到，虽然一级标题与段落确实有字体、字号的区分，但还不算美观。这是由浏览器根据标签的类别自动生成的**默认**外观，往往只能满足基本的阅读需要。

为了使得生成的网页更佳美观，可以考虑给不同格式的内容附加**样式**，就像在 Word 中改变标题、段落的样式一样。与 Word 中靠按钮操作不同，在网页中需要使用样式表（style sheet）来完成这些工作。目前，网页上通用的样式表格式被称为层叠样式表（cascading style sheet, CSS）。

CSS 的语法非常简单，针对以上 HTML 样例设计样式的 CSS 如下所示：

```css
h1 {
    text-align: center;
    border-bottom: 1px solid black;
}

p {
    font-family: '思源宋体';
    letter-spacing: 2px;
}

```

可以观察一下，以上共有两条 CSS **规则**（rule），每条规则由一个**选择器**（selector，用法很多，最简单的就是如本处一样直接用标签名指定某类标签）和一组由大括号包围的**声明**（declaration）构成。每一条声明会指定一种样式的改变。例如：

- 第一段是关于一级标题 `h1` 的规则：
  - 首先要求将其文本的对齐方式（`text-align`）固定为居中（`center`）；
  - 然后为其附加一条 1 像素宽（`1px`）、黑色的实线（`solid`）作为其下侧边框（`border-bottom`）。
- 第二段是关于段落 `p` 的规则：
  - 首先要求将字体族（`font-family`）固定为单一的「思源宋体」字体；
  - 然后将字距（`letter-spacing`）设定为 2 像素。

以上规则可通过多种方式加载到 HTML 文件中，包括：

- 将以上代码保存为 CSS 文件，并在 HTML 的 `head` 标签中用 `link` 标签将其与网页关联起来；
- 在 HTML 内合适位置使用 `style` 标签包含以上代码；
- 在浏览器的工具中载入以上代码。

载入后的效果如下所示，可见各条对样式的修改意见都得到了体现。

![img](2.png)

CSS 的内容非常庞杂，如选择器就有诸多组织方式（如按类、按 ID 选择标签，嵌套选择，按从属层级选择等——这是 CSS 中最有趣也最令人头疼的内容），可供声明、修改的样式也非常之多。选取个别内容系统学习是必要的，包括：

- 选择器的语法；
- 几何尺寸参数（位置、长宽、`padding`、`margin` 与 `border`）的意义、联系与区别；
- 样式表的堆叠优先级——当有多条相互冲突的规则时，浏览器会按照规则的来源、选择器内容、声明内容等进行「堆叠」，保证最终总是采纳某一条规则；
- 样式表如何载入到 HTML 页面中。

而除此以外的其他具体知识——如常用的样式规则等——可在平时浏览网页时零碎学习，或根据网页设计的需要现查先用。起初这会占用不少时间，但大多常用的内容你会很快地牢记在心。

> 事实上，网页样式的设计要比实现更重要：大多数人都能通过学习、尝试实现某种样式，但样式的设计并非人人所长。~~比如本文作者就永远也设计不出好的网页样式。~~

可以从[菜鸟教程](<https://www.runoob.com/css/css-tutorial.html>)开始学习 CSS，入门后的教程则请自行查找。另外，如果对干枯的代码感到苦恼，不妨到各个视频网站（如 bilibili）上寻找一些网页制作的实例。

稍后会说明如何在浏览器中对任意的网页检查其样式表，并现场做调整。

### 1.3 脚本：JavaScript 语言

通过 HTML 与 CSS 设计出来的网页，可以有非常精良的视觉效果，但缺少与用户**交互**的能力。不说在线答题、网页游戏等需要使用到数据库（这通常被称作「后端技术」）的复杂功能，即使是简单者如：

- 点击网页中的一个按钮，网页内容或样式能够发生改变，如许多网站的「主题」功能；
- 文章的字数统计；
- 自动拓展的菜单栏、滚动的图片展示框等；
- 自动生成的网页目录，并能够通过单击目录项跳转到网页对应位置……

也无法只用 HTML 与 CSS 来实现。为此，只有专门的编程才能实现对应功能；而在网页上使用的这种编程语言，被称作脚本语言（但它不止被用于网页上）。与 C++ 等需要编译为程序的编程语言不同，在网页上运行的脚本语言必须具有如下特性：

- 不需要编译，解释执行，以便加快代码的运作效率——如果需要编译的语言可理解为工厂经多道工序加工产品，则脚本语言恰如演员的「脚本」（台词），拿到脚本就能够开始演戏，不需要准备任何额外的工具、副产品。
- 语法简单，特别是在实现复杂功能时；
- 在用户的浏览器上执行（而不是在服务器中）——换句话说，虽然代码存放在网站上，但最终执行脚本的却是浏览网站者的浏览器。

网页中使用的脚本语言是 JavaScript（常简称 JS，它和 Java 没太大关系）。在将网页视为若干离散的「静态」文档时，脚本语言是不必要的；若需要利用若干网页构建一个具有若干功能的网站时，脚本语言不可或缺。

本教程不要求学习 JavaScript，有兴趣者可从[菜鸟教程](<https://www.runoob.com/js/js-tutorial.html>)开始了解相关知识。学习的方法很简单：像学其他编程语言（如学校教给你的 C 语言或 C++）一样就行。另外，如果你只希望学一点简单的脚本来操控 HTML、CSS 等网页内容，则可以快速了解 [JQuery](https://www.runoob.com/jquery/jquery-tutorial.html) 的用法，而不用从头、完整学习一门新的语言。

### 1.4 使用浏览器开发者工具

学习前端技术有诸多便利条件，最大的便利条件就是：你可以通过浏览器获取到所有网站的 HTML、CSS 与 JavaScript 源代码——当然，只是部分地获取到，因为网站中常有自动生成、加密或从服务器载入（大多不可见）的成分。当我们重点关注网页的内容、样式时，善用浏览器将带来很大帮助。

> 以下工作，请尽量使用**主流浏览器**完成，包括：Chrome、Firefox、Safari、最新的 IE 或 Edge，以及使用 Chromium 内核的浏览器（如 Opera 或 360 极速）。推荐 Chrome 或 Firefox。

浏览器的开发者工具一般使用 <kbd>F12</kbd> 或 <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd> 键打开，你也可以查找自己浏览器的解决方案。以 Chrome 浏览器为例，在网页上打开后将有多个组成部分，分别显示当前网页的 HTML 源代码与选中部分上叠加的所有 CSS 规则。

![img](3.png)

通过开发者工具中左上角处有鼠标图样的按钮，可以在网页中选择特定的元素（标签），检查其代码与样式。同时，还可以在样式表规则区域中，调整现有的规则，或增加新的规则——当然，这些仅在你的浏览器中生效，一旦网页刷新将抹去所有的修改。

通过浏览器的开发者工具，你可以了解到他人的网页是如何使用不同的 HTML 元素与 CSS 规则来实现精美视觉效果的。在学习前端技术的过程中，应多多使用。

### 1.5 Markdown 与 HTML 什么关系？

Markdown 最初就是为了简化 HTML 的写法而提出，其目的在于使用户用更简短的方式来填写网页的内容（不包括网页的其他部分，如附加信息、样式等）。因此，可以说，每一份 Markdown 文件的宿命就是**转换为 HTML**。

> 不过，转换的工作可以由不同的环节来实现。若你使用 Markdown 写笔记，则实现转换的是你的编辑软件、笔记网站；若你是用 Markdown 来搭建网站，则转换的是对应的网站生成工具；还有些情况下，由你主动完成转换 Markdown 的工作。
>
> 此外，现在人们也发现，Markdown 还可以作为 LaTeX 排版稿（以及由其生成的 PDF 文件）、Wiki 词条（使用 MediaWiki 格式）、Word 文档等其他许多文档格式的来源。原因很简单：Markdown 语法简单，容易编写，也容易转换。

Markdown 中的各种格式，在 HTML 中都有不同类别的标签一一对应。但反过来并不成立，因为 Markdown 只关注网页的内容（特别是文本内容），对于 HTML 能够实现的布局、元素嵌入、表单、复杂多媒体（如音乐、视频、游戏）等诸多功能并不关注。

常存在着一种对 Markdown 的误解，认为在网页上看到的、使用 Markdown 实现的笔记、文章等就是 Markdown 文档。事实上，在浏览器乃至许多编辑器、软件中所看到的「Markdown」文档，实际上都是经过转换的 HTML 文件，后者才具有样式，前者不过是特定规则组合而成的代码字符而已。换句话说，我们能看见的简洁、精美的「Markdown 文档」，实际上是 HTML 格式的、并附带有 CSS 样式表的 Markdown 效果**预览**。

由于 Markdown 与 HTML 的关系，现在已有越来越多的网站（的文章正文）使用 Markdown 而非 HTML 编写，并通过一套自动化构件将 Markdown 转换为 HTML 代码，直接填入到网页之中。这样，即使是不懂前端技术的人，也可以通过 Markdown 代码在他人设计好的样式下轻松构建一个网站。上一周大家初步尝试的 GitHub Pages 即是如此。

## 2 任务

本周的学习内容比较繁多，前端技术比起 Markdown 来要困难不少，并且没有明确的边界。因此，这里给出一个开放性的任务——复刻网页，供大家学习的同时操练。

如果你之前已经通过 GitHub Pages 生成了一个简单的网页，并套用了网站提供的某一个主题，请打开其中的任意一个网页，尝试着自己从头搭建这个页面的 HTML 框架与 CSS 样式表。

这个任务是开放性的：你可以不时参考原网页的代码，也可以在原来的样式上做出你自己喜欢的改动。此项任务的目的不在于替换原来的样式，而在于：

- 熟悉一个网页的组成部分；
- 了解样式表的构建方式，学会调整已有样式或设计新样式；
- 推断一个 Markdown 文件**如何**变成了一个网页（下一周的课程中会介绍）。
